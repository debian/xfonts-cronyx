Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Xcyr
Source: http://sawsoft.newmail.ru/LS/xcyr-2.3.tar.gz
Comment: The original packaging said:
         This is Debian GNU/Linux prepackaged version of the font collection
         `Xcyr' ver. 2.3.7 by Serge Winitzki. `Xcyr' is a descendant of the
         package Xrus-2.3 by Andrey Chernov.
         .
         For details about the excluded file below, see #1016557.
Files-Excluded: tryfont/ISO8859-5

Files: *
Copyright: 1989-1991 Network Computing Devices, Inc.
           1990-1991 EWT Consulting
           1994      Aleksei Rudnev <alex@kiae.su>
           1994-1995 Cronyx Ltd.
           1995      EdicT Software <edict@cbtver.tmts.tver.su>
           1996-1997 Andrey A. Chernov
           1996      Sergey Vovk
           1999-2000 Serge Winitzki
License: Special

Files: bdf2fnt.pl
       bdf2xbm.pl
       bdftrim.pl
       misc/koi6x9.bdf
       misc/koi6x13b.bdf
       misc/koi7x14.bdf
       misc/koi7x14b.bdf
       misc/koi8x13.bdf
       misc/koi9x15.bdf
       misc/koi9x15b.bdf
       misc/koi9x18.bdf
       misc/koi9x18b.bdf
       misc/proof9x16.bdf
       misc/screen9x18.bdf
Copyright: 1999      Markus Kuhn <mkuhn@acm.org>
           1999-2001 Serge Winitzki
License: Public-Domain

Files: debian/*
Copyright: 2000-2016 Anton Zinoviev <zinoviev@debian.org>
           2019      Boyuan Yang <byang@debian.org>
           2021      Jelmer Vernooĳ <jelmer@debian.org>
           2021-2022 Joao Eriberto Mota Filho <eriberto@debian.org>
License: GPL-2+

License: Special
 This software may be used, modified, copied, distributed, and sold,
 in both source and binary form provided that the above copyright
 and these terms are retained. Under no circumstances is the author
 responsible for the proper functioning of this software, nor does
 the author assume any responsibility for damages incurred with its use.

License: Public-Domain
 This file is under Public Domain.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
