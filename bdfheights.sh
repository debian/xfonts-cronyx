#!/bin/sh

# Check all BDF fonts for mismatched X_HEIGHT and CAP_HEIGHT, modify them in place if necessary

dir="."	# Root of xcyr distribution

tadd="/tmp/addlines.tmp"	# Lines to be added to fonts

for font in $dir/*/*.bdf; do

# Find out if the font has X_HEIGHT and CAP_HEIGHT
fontxheight=`grep X_HEIGHT "$font" | sed -e 's/X_HEIGHT //'`
fontcapheight=`grep CAP_HEIGHT "$font" | sed -e 's/CAP_HEIGHT //'`
xheight=`perl -e 'while(<>){last if (/STARTCHAR x/);} while(<>){if (/BBX [0-9]+ ([0-9]+) /) {print $1; last;}}' < "$font"`
capheight=`perl -e 'while(<>){last if (/STARTCHAR X/);} while(<>){if (/BBX [0-9]+ ([0-9]+) /) {print $1; last;}}' < "$font"`

# Number of properties
props=`grep STARTPROPERTIES "$font" | sed -e 's/STARTPROPERTIES //'`

if [ x"$fontxheight"y"$fontcapheight" = x"$xheight"y"$capheight" ]
then
echo "Font '$font' is okay"
else
	echo "Font '$font': x='$fontxheight', cap='$fontcapheight', real x='$xheight', real cap='$capheight'"

	newprops=$props

	rm -f $tadd

	if [ x"$fontxheight" = x ]
	then
		newprops=`expr $newprops + 1`
#		echo "X_HEIGHT $xheight" >> $tadd
	fi

	if [ x"$fontcapheight" = x ]
	then
		newprops=`expr $newprops + 1`
#		echo "CAP_HEIGHT $capheight" >> $tadd
	fi
	sed -e 's/STARTPROPERTIES.*$/STARTPROPERTIES '$newprops/ < "$font" | grep -v X_HEIGHT | grep -v CAP_HEIGHT | perl -e 'while(<>){last if (/ENDPROPERTIES/);print;} print"X_HEIGHT '$xheight'\nCAP_HEIGHT '$capheight'\n$_"; while(<>){print;}' > $tadd
	mv $tadd "$font"
fi

done
