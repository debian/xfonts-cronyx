#!/bin/sh

# Prepare a distribution with all fonts changed, recoded etc. Sample file that does nothing to the current distribution but is useful after some XMBDFED editing, because it may add zero padding to bitmaps or omit RANGES.

# Directory to prepare files in.
targetdir="./xcyr-2.3-new"

# Prepare that directory.
./mktargetdir.sh "$targetdir"

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf	# Want to do something else with fixed fonts
do
    perl bdfrecode.pl -table=full.table -usenames=unicode.names -registry=KOI8-C < $font | perl bdftrim.pl > $targetdir/$font
    echo "$font"
done

for font in 100dpi/*.bdf 75dpi/*.bdf
do
    perl bdfrecode.pl -table=full.table -usenames=unicode.names -registry=KOI8-C < $font | perl bdftrim.pl > $targetdir/$font
    echo "$font"
done

# Transform the special font "koinil2.bdf"

perl bdfrecode.pl -table=full.table -registry=KOI8-C < misc/koinil2.bdf | perl bdftrim.pl > $targetdir/misc/koinil2.bdf

echo Done.
