#!/bin/sh

# Prepare a stripped-down xrus distribution.

. version.sh

# Directory to prepare xrus files in.
targetdir="./xukr-$version"

# Prepare that directory.

./mktargetdir.sh "$targetdir"

# Transform all fonts.

echo Recoding fonts:
for font in misc/*.bdf 100dpi/*.bdf 75dpi/*.bdf
do
    perl bdfrecode.pl -table=xcyr2xukr.txt -registry=KOI8-U < $font > $targetdir/$font
    echo "$font"
done

# Transform "fonts.alias" in the "misc" directory
perl -e 'while(<>){s/koi8-c/koi8-u/g;print;}' < misc/fonts.alias > "$targetdir"/misc/fonts.alias

# Transform the special font "koinil2.bdf"

perl bdfrecode.pl -table=full.table -registry=KOI8-U < misc/koinil2.bdf > $targetdir/misc/koinil2.bdf

echo Done.




