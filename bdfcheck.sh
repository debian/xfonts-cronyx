#!/bin/sh

# Check all BDF fonts for mismatched widths of similar characters

dir="."	# Root of xcyr distribution

for font in $dir/*/*.bdf; do
for chars in "101 163 197" "164 220" "115 165" "131 173 199" "174 213" "179 229" "180 252" "83 181" "129 189 231" "190 245" "141 235" "157 203"; do
	result=""
	result=`eval bdfcmpc.pl $chars < $font`
	[ x"$result" != x ] && echo "$font : $result"

done
done



