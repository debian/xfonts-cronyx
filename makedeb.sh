#!/bin/sh

# Make Debian package for xcyr fonts
# Need generated tar.gz of a distribution

echo "Attempting to create a Debian package..."

. version.sh

pack="$packagename-$version.tar.gz"

confdir="etc/X11/fonts/$packagedir"
conffile="$debname.alias"

docdir="usr/share/doc/$packagedir"
tmpdir="/tmp/makedeb.tmp$$"
target="$tmpdir/usr/X11R6/lib/X11/fonts"

[ -r $pack ] || {
	echo "Error: Fonts archive '$pack' missing or unreadable."
	exit 1
}

rm -rf "$tmpdir"
mkdir -p "$tmpdir"/DEBIAN "$target" "$tmpdir/$docdir" 

gzip -d < "$pack" | tar -C "$target" -xf -
# Move fonts.alias
mkdir -p "$tmpdir"/"$confdir"
mv "$target/$packagedir/misc/fonts.alias" "$tmpdir/$confdir/$conffile"
# Move all "misc" fonts into main dir
mv "$target/$packagedir"/misc/* "$target/$packagedir"
rmdir "$target/$packagedir"/misc/

cp DEBIAN/copyright Changelog.en *README* "$tmpdir/$docdir"

thesize=`du -k -s "$tmpdir"|cut -f1`

cp DEBIAN/{conffiles,postinst,postrm,prerm} "$tmpdir"/DEBIAN/

cat DEBIAN/control | sed -e "s/debversion/$version/;s/Installed-Size: .*$/Installed-Size: $thesize/" > "$tmpdir"/DEBIAN/control

dpkg-deb -b "$tmpdir" .

rm -rf "$tmpdir"
